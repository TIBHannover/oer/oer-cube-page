---
title: OER veröffentlichen
subtitle: In einem öffentlichen Repositorium bereitstellen
date: 2019-07-11
tags: ["open educational resources", "oer"]
---

### TBD

Mit der Bereitstellung der Materialien im Netz sind diese bereits öffentlich verfügbar. Um sie nach einheitlichen Kriterien an einer zentralen Stelle besser finden zu können, bieten sich entsprechende Repositorien an. Eine Zusammenstellung passender Repositorien ist noch in Arbeit.

Eine Checkliste für die Veröffentlichung gibt es bereits unter [WB-WEB.de](https://wb-web.de/material/medien/ich-mochte-eigene-materialien-als-oer-zur-verfugung-stellen.html)

<!--more-->
