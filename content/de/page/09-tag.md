---
title: OER beschreiben
subtitle: Mit Metadaten auffindbar machen
date: 2019-07-09
tags: ["open educational resources", "oer"]
---

Damit die Materialien die man als OER freigibt auch gefunden werden, müssen diese mit ein paar zusätzlichen Informationen versehen werden.

### TBD

Wie Metadaten im Rahmen dieser Anleitung am einfachsten zu verwenden sind, wird gerade noch erarbeitet. Mögliche Suchkriterien werden z.B. sein:

* Fach
* Thema
* Dauer
* Schwierigkeit
* Abhängigkeiten
* ...

<!--more-->
