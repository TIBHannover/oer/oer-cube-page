---
title: OER remixen
subtitle: Eigene Kurse oder Module zusammenstellen
date: 2019-07-08
tags: ["open educational resources", "oer"]
---

### Einfacher Einstieg

Beim Remixen von OER geht es darum, mit Materialien aus verschiedenen Quellen und mit unterschiedlichen Lizenzen neue Inhalte zusammen zu stellen. Dabei ist zu beachten, dass sich nicht alle Lizenzen mit einander kombinieren lassen, weil mitunter die eine Lizenz die Freiheiten einer anderen Lizenz ausschließt.

Um dennoch möglichst einfach starten zu können, beginnen wir an dieser Stelle damit, zunächst nur CC-0 lizensierte Inhalte zu verwenden. Bei weiterer Betrachtung lassen sich auch andere Materialien miteinander kombinieren. Hierzu mehr unter weitere Informationen.

### Plattform zum Remixen

Es gibt verschiedene Plattformen, auf denen sich Kursmaterialien zusammenstellen lassen. In diesem Beispiel verwenden wir [GitLab](https://gitlab.com). GitLab ist eigentlich eine verteilte Versionsverwaltung für Quelltexte in der Softwareentwicklung, kann aber genauso gut für die gemeinsame Erstellung und Bearbeitung von Bildungsmaterialien genutzt werden.

Die Schritte auf dem Weg zu einer eigenen Unterrichteinheit sind wie folgt:

* Benutzerkonto erstellen
* Projekt erstellen
* Eine Datei erstellen - im einfachsten Fall zunächst README.md
* Inhalte mit [Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) erstellen. Neben Überschriften, Absätzen, Listen und Tabellen lassen sich hier auch Bilder und Videos in den Text einbetten

### Weitere Informationen

Tips zum Remixen von Materialien mit verschiedenen CC-Lizenzen [iRights](https://irights.info/artikel/kombinieren-bearbeiten-remixen-oer-richtig-verwenden/28560).

{{< youtube mZwEH3VHcfM >}}


{{< figure src="/images/IFLA_Wuerfel_9.jpg" width="400px" >}}

<!--more-->
