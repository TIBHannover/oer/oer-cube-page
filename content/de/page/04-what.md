---
title: Was sind OER
subtitle: Eine allgemeine Definition
date: 2019-07-04
tags: ["open educational resources", "oer"]
---

Freie Bildungsmaterialien nach der Definition der UNESCO:

>  Open Educational Resources (OER) sind Bildungsmaterialien jeglicher Art und in jedem Medium, die unter einer offenen Lizenz stehen. Eine solche Lizenz ermöglicht den kostenlosen Zugang sowie die kostenlose Nutzung, Bearbeitung und Weiterverbreitung durch Andere ohne oder mit geringfügigen Einschränkungen. Dabei bestimmen die Urheber selbst, welche Nutzungsrechte sie einräumen und welche Rechte sie sich vorbehalten.  
Quelle: [Deutsche UNESCO-Kommision](https://www.unesco.de/bildung/open-educational-resources)

{{< figure src="/images/IFLA_Wuerfel_3.jpg" width="400px" >}}

<!--more-->
