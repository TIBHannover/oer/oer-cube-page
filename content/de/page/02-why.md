---
title: Warum OER verwenden und erstellen
subtitle: Warum sollten wir unsere Bildungsmaterialien freigeben?
date: 2019-07-02
tags: ["open educational resources", "oer"]
---

Mit OER können Bildungsmaterialien genutzt, bearbeitet, ergänzt oder erstellt werden. Unterschiedliche Plattformen auf der ganzen Welt bieten OER an und viele Suchmaschinen ermöglichen eine gezielte Lizenzsuche. OER sind nicht nur für Lehrende gewinnbringend, sondern auch für Lernende. OER bietet verschiedene Vorteile:

* OER steht jedem weltweit zur Verfügung
* Durch Digitalisierung und Vernetzung kann das Material weit verbreitet und ausgetauscht werden
* OER ermöglicht eine neue Art der Zusammenarbeit
* Chancengleichheit im Bereich Bildung
* Zeitersparnis beim Austausch und Wiederverwendung von Lehr-/Lernmaterialien
* Gemeinsam können Bildungsmaterialien erstellt, aktualisiert, bearbeitet und perfektioniert werden

Weitere Informationen unter [OERinfo – die Informationsstelle OER](http://www.open-educational-resources.de)

<!--more-->

## Eine kurze Einführung

{{< vimeo 51902327 >}}
