---
title: Mach Bildung frei für alle!
subtitle: Eine globale Anforderung
date: 2019-07-01
tags: ["open educational resources", "oer"]
---

Offene Bildungsressourcen im Sinne der [UNESCO](https://www.unesco.de/bildung/open-educational-resources) haben das Potenzial, eine qualitativ hochwertige Bildung für alle zu unterstützen, wie es von den [Zielen für nachhaltige Entwicklung](https://www.unesco.de/bildung/bildungsagenda-2030) angestrebt wird.

{{< figure src="/images/IFLA_Wuerfel_.jpg" width="400px" >}}

<!--more-->
