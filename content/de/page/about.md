---
title: Über diese Seite
subtitle: Das Projekt "OER-Würfel"
comments: false
---

Die Idee hinter dem OER-Würfel ist, neben den zahlreichen Anleitungen zu OER einen kompakten Praxis-Leitfaden für den Einstieg zu erstellen, mit Hilfe dessen Lehrende und Lernende sich innerhalb von 5 Minuten einen Überblick verschaffen und in weniger als einer Stunde praktisch einsteigen können. Beim Einstieg sollte der gesammte Lebenszyklus von OER einmal exemplarisch durchgespielt werden können.
