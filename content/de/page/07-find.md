---
title: OER finden
subtitle: Wie finde ich freie Bildungsmaterialien?
date: 2019-07-07
tags: ["open educational resources", "oer"]
---

### Einfacher Einstieg

Keine einschränkenden Lizenzen - Public Domain / CC-0

Freie Bildungsmaterialien je nach Art im Internet finden. Im Rahmen dieses Leitfadens beschränken wir uns zunächst auf Materialien ohne Urheberrechtseinschränkungen. Dies engt zwar die Auswahl ein, aber wir müssen keine Bedenken haben, dass wir sie rechtlich falsch verwenden könnten.

* **[Flickr](https://www.flickr.com)** - vor allem Fotos
  * Suche nach z.B. "Pythagoras"
  * Erweitert -> Lizenz -> Keine bekannten Urheberrechtseinschränkungen
* **[Google](https://www.google.com)** - alle arten von Materialen, hier: Bilder
  * Suche nach z.B. "Satz des Pythagoras"
  * Bilder -> Tools -> Nutzungsrechte -> Zur Wiederveränderung und Veränderung gekennzeichnet
* **[Youtube](https://www.youtube.com)** - Videos
  * Suche nach z.B. "Satz des Pythagoras"
  * Filter -> Eigenschaften: Creative Commons

Wenn wir keine passenden Materialien mit CC-0 Lizenz finden, können wir zunächst selbst beispielsweise Grafiken skizzieren, entweder analog und abfotografieren oder gleich digital erstellen, und diese auf https://commons.wikimedia.org hochladen und mit CC-0 versehen.

Bilder und Videos können zwar, sofern sie nicht allzu groß sind, auch direkt auf einer Wiki-Seite oder in GitLab gespeichert werden, aber die Ablage auf einer Bilder- oder Videoplattform hat mitunter den Vorteil, dass dort auch maschinenlesbare Lizenzen angegeben werden können, und die Inhalte damit auch über generische Plattformen wie Google gefunden werden können.  

### Weitere Materialien

Creative Commons - frei verwendbar, aber mit Einschränkungen

Wenn die Materialien nur im Unterricht an öffentlichen Einrichtungen und nicht kommerziell verwendet/eingebunden und nicht verändert werden sollen, dann kann man die Auswahl auch auf weitere Lizenzen ausdehnen

* Suche nach CC BY, CC BY SA, +ND, +NC
* **[Wikimedia Commons](https://commons.wikimedia.org)** - Audio, Video, Image, Document
  * Suche nach z.B. "Satz des Pythagoras"

Ausführliche Informationen unter [wb-web.de](https://wb-web.de/material/medien/Wo-und-wie-finde-ich-Open-Educational-Resources.html)

{{< figure src="/images/IFLA_Wuerfel_7.jpg" width="400px" >}}

{{< figure src="/images/IFLA_Wuerfel_8.jpg" width="400px" >}}


<!--more-->
