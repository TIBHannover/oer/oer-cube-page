---
title: Wer profitiert von OER
subtitle: Wer benutzt und erstellt OER?
date: 2019-07-03
tags: ["open educational resources", "oer"]
---

Freie Bildungsmaterialien bieten sowohl Lernenden als auch Lehrenden viele Vorteile.

**Lernende**

* alles lernen, was sie brauchen, was sie interessiert, was sie weiterbringt
* Orientierung über mögliche Lernwege bekommen
* Ergänzung zu aktuellen Lernzielen

**Lehrende**

* auf vorhandenes Material zugreifen
* Materialien gemeinsam bearbeiten und verbessern
* Nachhaltigkeit der eigenen Arbeit sicherstellen

Jeder kann Bildungsmaterialien erstellen und frei verfügbar machen. Hier nur ein paar Beispiele:

**Lernende**

* Hausarbeiten
* Klausurvorbereitungen
* Übungsaufgaben
* ...

**Lehrende**

* Vorlesungsmaterialien
* Klausurvorbereitungen
* Aufgaben mit Losungen und Lösungswegen
* ...


{{< figure src="/images/IFLA_Wuerfel_2.jpg" width="400px" >}}

<!--more-->
