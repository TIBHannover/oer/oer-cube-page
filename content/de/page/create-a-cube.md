---
title: Würfel selber bauen
subtitle: Grafiken zum Selbstbau eines Würfels
date: 2019-07-12
tags: ["infrastructure", "oer"]
---

## Die Grafiken für den Nachbau eines OER-Faltwürfels

{{< figure src="/images/Wuerfel-DE-1b.jpg" width="400px" >}}

### Die Außenseiten

Oben

{{< figure src="/images/IFLA_Wuerfel_.jpg" width="400px" >}}

Vorne

{{< figure src="/images/IFLA_Wuerfel_2.jpg" width="400px" >}}

Rechts

{{< figure src="/images/IFLA_Wuerfel_3.jpg" width="400px" >}}

Hinten

{{< figure src="/images/IFLA_Wuerfel_4.jpg" width="400px" >}}

Links

{{< figure src="/images/IFLA_Wuerfel_5.jpg" width="400px" >}}

Unten

{{< figure src="/images/IFLA_Wuerfel_6.jpg" width="400px" >}}

### Die Innenseiten

Oben

{{< figure src="/images/IFLA_Wuerfel_7.jpg" width="400px" >}}

Vorne

{{< figure src="/images/IFLA_Wuerfel_8.jpg" width="400px" >}}

Rechts

{{< figure src="/images/IFLA_Wuerfel_9.jpg" width="400px" >}}

Hinten

{{< figure src="/images/IFLA_Wuerfel_10.jpg" width="400px" >}}

Links

{{< figure src="/images/IFLA_Wuerfel_11.jpg" width="400px" >}}

Unten

{{< figure src="/images/IFLA_Wuerfel_12.jpg" width="400px" >}}

<!--more-->
