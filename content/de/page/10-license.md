---
title: OER lizenzieren
subtitle: Erlaube anderen die Materialien zu nutzen
date: 2019-07-10
tags: ["open educational resources", "oer"]
---

### TBD

Bei der Lizenzierung von eigens erstellten oder remixten OER gilt es, ein paar grundlegende Dinge zu beachten. Eine Anleitung im Rahmen dieser Leitlinie ist noch in Arbeit.

Einige Informationen dazu finden sich bereits unter [WB-WEB.de](https://wb-web.de/material/medien/die-cc-lizenzen-im-uberblick-welche-lizenz-fur-welche-zwecke-1.html)

{{< figure src="/images/IFLA_Wuerfel_10.jpg" width="400px" >}}

<!--more-->
