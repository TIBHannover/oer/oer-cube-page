---
title: Wie benutzt und erstellt man OER
subtitle: Ein beispielhafter Ablauf
date: 2019-07-05
tags: ["open educational resources", "oer"]
---

Die wesentlichen Schritte im Kreislauf der OER

* Finden freier Materialien mit offenen Lizenzen
* Zusammenstellen größerer Einheiten
* Auszeichnen der Daten mit Schlagworten und Metadaten zur besseren Auffindbarkeit
* Lizensierung der erstellten Materialien
* Bereitstellung in öffentlichen Repositorien

<!--more-->
