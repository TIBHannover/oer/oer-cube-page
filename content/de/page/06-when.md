---
title: Wann anfangen
subtitle: Wann kann ich mit OER beginnen?
date: 2019-07-06
tags: ["open educational resources", "oer"]
---

Jetzt gleich! Los gehts ...

### Was brauche ich dafür?

Im einfachsten Fall nur einen Computer und Zugang zum Internet. Ggf. für eigene Grafiken noch ein paar Stifte, Papier und ein Handy mit Kamera.

Bei weiterer Vertiefung mit OER kann mann natürlich auch später noch bei Bedarf weitere Werkzeuge einsetzen, aber im Rahmen dieser Einführung brauchen wir nicht mehr.

{{< figure src="/images/IFLA_Wuerfel_6.jpg" width="400px" >}}

<!--more-->
