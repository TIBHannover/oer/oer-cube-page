# Impressum

Impressum für diese Website - dient zugleich als Anbieterkennzeichnung nach § 5 Telemediengesetz (TMG)

### Anbieter

Technische Informationsbibliothek (TIB)  
Welfengarten 1 B  
30167 Hannover

### Bevollmächtigter Vertreter

Prof. Dr. Sören Auer (Director of TIB)  
Die Technische Informationsbibliothek (TIB) ist eine Stiftung des öffentlichen Rechts des Landes Niedersachsen.

### Zuständige Aufsichtsbehörde

Niedersächsisches Ministerium für Wissenschaft und Kultur

### Kontakt

Axel Klinger  
Fax: +49 511 762-4076  
Email: axel (dot) klinger (at) tib (dot) eu

### Umsatzsteuer-Identifikationsnummer

DE 214931803

### Redaktion

Axel Klinger  
Email: axel (dot) klinger (at) tib (dot) eu

### Urheberrecht

Das Layout dieser Website wird von [Hugo](https://gohugo.io/) erstellt und ist unter MIT lizenziert. Alle Bilder der Website sind frei verwendbar, mit Ausnahme der integrierten Grafiken für das OER-Logo, das SDG4-Logo und die Grafiken für die Lizenzen, die mit separaten Lizenzen gekennzeichnet sind.
