## Der OER Würfel

### *DIESE SEITE IST NOCH IM AUFBAU!*

{{< figure src="/images/Wuerfel-DE-1b.jpg" width="400px" >}}

Ein paar einleitende Informationen (5min)

* [Open Education](page/01-title)
* [Warum open education?](page/02-why)
* [Wer profitiert von OER?](page/03-who)
* [Definition von OER](page/04-what)
* [Wie finde und produziere ich OER](page/05-how)
* [Wann anfangen?](page/06-when)

Konkrete Handlungsanweisungen (50min)

* [OER Finden](page/07-find)
* [OER Remixen](page/08-remix)
* [OER Taggen](page/09-tag)
* [OER Lizensieren](page/10-license)
* [OER Veröffentlichen](page/11-publish)
* [Gratulation!](page/12-congrat)

Neben diesen Punkten

* [Eine beispielhafte OER Infrastruktur](page/infrastructure)
* [Dienste von Bibliotheken für OER](page/library-services)
* [Diesen Würfel selbst bauen](page/create-a-cube)

Diese Seite basiert auf der Originalskizze für einen [OER-Würfel](https://gitlab.com/axel-klinger/oer-wuerfel)
