---
title: Why use and produce OER
subtitle: Why should we open our educational resources?
date: 2019-07-02
tags: ["open educational resources", "oer"]
---

With OER, educational materials can be used, edited, supplemented or created. Different platforms all over the world offer OER and many search engines allow a targeted license search. OER are not only profitable for teachers, but also for learners. OER offers several advantages:

* OER is available to everyone worldwide
* Digitisation and networking allows material to be widely disseminated and exchanged
* OER enables a new kind of collaboration
* Equal opportunities in education
* Time savings when exchanging and reusing teaching/learning materials
* Together, educational materials can be created, updated, edited and perfected

<!--more-->

## A short introduction

{{< vimeo 43401199 >}}
