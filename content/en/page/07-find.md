---
title: Find OER
subtitle: Find open resources
date: 2019-07-07
tags: ["open educational resources", "oer"]
---

No restrictive licenses - Public Domain / CC-0

Find free educational materials on the Internet, depending on the type. Within the scope of this guide, we limit ourselves initially to materials without copyright restrictions. This restricts the choice, but we don't have to worry that we might misuse them legally.

* **[Flickr](https://www.flickr.com)** - especially photos
  * Search for "Pythagoras", for example.
  * Advanced -> License -> No known copyright restrictions
* **[Google](https://www.google.com)** - all kinds of material, here: Pictures
  * Search for e.g. "Pythagorean theorem."
  * Images -> Tools -> Rights of use -> Flagged for re-editing and modification
* **[Youtube](https://www.youtube.com)** - Videos
  * Search for e.g. "Pythagorean theorem."
  * Filter -> Properties: Creative Commons

If we can't find any suitable material with CC-0 license, we can sketch graphics ourselves, for example, either analog and photograph or create them digitally, and upload them to https://commons.wikimedia.org and add CC-0 to them.

### Other materials

Creative Commons - freely usable, but with limitations

If the materials are to be used/incorporated only in teaching at public institutions and not for commercial purposes and should not be modified, then the selection can also be extended to other licenses.

* Search for CC BY, CC BY SA, +ND, +NC
* **[Wikimedia Commons](https://commons.wikimedia.org)** - Audio, Video, Image, Document
  * Search for e.g. "Pythagorean theorem."


{{< figure src="/images/IFLA_Wuerfel_7.jpg" width="400px" >}}

{{< figure src="/images/IFLA_Wuerfel_8.jpg" width="400px" >}}

<!--more-->
