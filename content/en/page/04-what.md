---
title: What are OER
subtitle: A global definition
date: 2019-07-04
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO

> Open Educational Resources (OER) are teaching, learning and research materials in any medium – digital or otherwise – that reside in the public domain or have been released under an open license that permits no-cost access, use, adaptation and redistribution by others with no or limited restrictions.  
Source: [UNESCO](https://en.unesco.org/themes/building-knowledge-societies/oer)

{{< figure src="/images/IFLA_Wuerfel_3.jpg" width="400px" >}}

<!--more-->
