---
title: About this page
subtitle: The Project "OER Cube"
comments: false
---

The idea behind the OER cube is to create a compact practical guide for beginners in addition to the numerous instructions for OER, with the help of which teachers and learners can get an overview within 5 minutes and get started practically in less than an hour. At the beginning, the entire life cycle of OER should be exemplary.
