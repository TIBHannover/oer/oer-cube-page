---
title: Tag OER with metadata
subtitle: Make it findable by categories
date: 2019-07-09
tags: ["open educational resources", "oer"]
---

In order for the materials that you release as OER to be found, they need to be provided with some additional information.

### TBD

The easiest way to use metadata in this manual is still being worked out. Possible search criteria will be, for example:

* Subject
* Theme
* Duration
* Difficulty
* Dependencies

<!--more-->
