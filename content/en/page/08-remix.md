---
title: Remix OER
subtitle: Create a course or course module
date: 2019-07-08
tags: ["open educational resources", "oer"]
---

### Easy entry

OER's remixing is about putting together new content with materials from different sources and with different licenses. It should be noted that not all licenses can be combined with each other, because sometimes one license excludes the freedom of another license.

In order to be able to start as easy as possible, we will start by using only CC-0 licensed content. If we take a closer look, other materials can also be combined with each other. For more information see further information.

### Platform for remixing

There are various platforms on which course materials can be compiled. In this example we use [GitLab](https://gitlab.com). GitLab is actually a distributed version management system for source code in software development, but it can also be used for creating and editing educational materials together.

The steps on the way to your own teaching unit are as follows:

* Create user account
* Create project
* Create a file - in the simplest case first README.md
* Create content with [Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/). In addition to headings, paragraphs, lists and tables, you can also embed images and videos in the text.

{{< figure src="/images/IFLA_Wuerfel_9.jpg" width="400px" >}}

<!--more-->
