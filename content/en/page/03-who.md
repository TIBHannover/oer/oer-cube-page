---
title: Who could benefit from OER
subtitle: Who can use and produce OER
date: 2019-07-03
tags: ["open educational resources", "oer"]
---

Open educational resources offer many advantages to both learners and teachers.

**Learners**

* learn everything they need, what interests them, what gets them ahead
* Get orientation about possible learning paths
* Supplement to current learning objectives

**Teachers**

* Access existing material
* Work and improve materials together
* Ensuring the sustainability of our own work

Anyone can create educational materials and make them freely available. Here are just a few examples:

**learners**

* Homework
* Exam preparations
* Exercises
* ...

{{< figure src="/images/IFLA_Wuerfel_2.jpg" width="400px" >}}



<!--more-->
