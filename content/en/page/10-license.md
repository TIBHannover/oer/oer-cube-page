---
title: License OER
subtitle: Give other people the rights to use it
date: 2019-07-10
tags: ["open educational resources", "oer"]
---

### TBD

When licensing OERs that you have created or remixed yourself, you need to consider a few basic things. An instruction within the scope of this guideline is still in progress.

{{< figure src="/images/IFLA_Wuerfel_10.jpg" width="400px" >}}

<!--more-->
