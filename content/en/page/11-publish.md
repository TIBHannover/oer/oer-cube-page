---
title: Publish OER
subtitle: Make it findable in a global repository
date: 2019-07-11
tags: ["open educational resources", "oer"]
---

### TBD

With the provision of the materials on the net, they are already publicly available. In order to be able to find them better at a central location according to uniform criteria, appropriate repositories are available. A compilation of suitable repositories is still in progress.

<!--more-->
