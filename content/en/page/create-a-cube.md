---
title: Create a cube
subtitle: Graphics for building a cube yourself
date: 2019-07-12
tags: ["infrastructure", "oer"]
---

## The graphics for the replica of an OER folding cube

{{< figure src="/images/Wuerfel-DE-1b.jpg" width="400px" >}}

### The outer pages

Top

{{< figure src="/images/IFLA_Wuerfel_.jpg" width="400px" >}}

Front

{{< figure src="/images/IFLA_Wuerfel_2.jpg" width="400px" >}}

Right

{{< figure src="/images/IFLA_Wuerfel_3.jpg" width="400px" >}}

Back

{{< figure src="/images/IFLA_Wuerfel_4.jpg" width="400px" >}}

Left

{{< figure src="/images/IFLA_Wuerfel_5.jpg" width="400px" >}}

Bottom

{{< figure src="/images/IFLA_Wuerfel_6.jpg" width="400px" >}}

### The inner pages

Top

{{< figure src="/images/IFLA_Wuerfel_7.jpg" width="400px" >}}

Front

{{< figure src="/images/IFLA_Wuerfel_8.jpg" width="400px" >}}

Right

{{< figure src="/images/IFLA_Wuerfel_9.jpg" width="400px" >}}

Back

{{< figure src="/images/IFLA_Wuerfel_10.jpg" width="400px" >}}

Left

{{< figure src="/images/IFLA_Wuerfel_11.jpg" width="400px" >}}

Bottom

{{< figure src="/images/IFLA_Wuerfel_12.jpg" width="400px" >}}

<!--more-->
