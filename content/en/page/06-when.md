---
title: When to start
subtitle: When can I start with OER
date: 2019-07-06
tags: ["open educational resources", "oer"]
---

Richt now! Let's go ...

### What do I need for it?

In the simplest case only a computer and access to the Internet. If necessary for own graphics still a few pens, paper and a mobile phone with camera.

If you want to deepen your knowledge with OER, you can of course use other tools later if you want, but we don't need any more within the scope of this introduction.

{{< figure src="/images/IFLA_Wuerfel_6.jpg" width="400px" >}}

<!--more-->
