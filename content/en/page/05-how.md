---
title: How to use and produce OER
subtitle: A short example workflow
date: 2019-07-05
tags: ["open educational resources", "oer"]
---

The essential steps in the OER cycle

* Find free materials with open licenses
* Assembling larger units
* Marking of the data with keywords and metadata for better findability
* Licensing of the created materials
* Publishing in public repositories

<!--more-->
