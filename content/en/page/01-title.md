---
title: Open Education for all!
subtitle: A global demand
date: 2019-07-01
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by [UNESCO](https://en.unesco.org/themes/building-knowledge-societies/oer) have the capability to support quality education for all as targeted by the [Sustainable Development Goals](https://sustainabledevelopment.un.org/sdg4).

{{< figure src="/images/IFLA_Wuerfel_.jpg" width="400px" >}}

<!--more-->
